#include "tests.h"
#include "mem.h"
#include "mem_internals.h"

#include <stdio.h>

#define HEAP_SIZE 16384
#define HEAP_SIZE_LAST 1024
#define MALLOC_1_TEST 2048
#define MALLOC_2_TEST1 1024
#define MALLOC_2_TEST2 512
#define MALLOC_3_TEST1 256
#define MALLOC_3_TEST2 128
#define MALLOC_3_TEST3 64
#define MALLOC_4_TEST1 64
#define MALLOC_4_TEST2 8193
#define MALLOC_5_TEST 8193

static void destroy_heap(void* heap, size_t size){
    munmap(heap, size_from_capacity((block_capacity){.bytes = size}).bytes);
}

static struct block_header* get_header(void* contents) {
    return (struct block_header*) ((uint8_t*) contents - offsetof(struct block_header, contents));
}

void firstTest(){
    puts("init the memory");

    void* heap = heap_init(HEAP_SIZE);
    if (heap == NULL){
        puts("fatal error when init the memory");
        return;
    }

    puts("success init memory");
    debug_heap(stdout, heap);


    void* test = _malloc(MALLOC_1_TEST);

    if (test == NULL){
        puts("fatal error when allocate the memory");
        return;
    }

    puts("after allocating the memory");
    debug_heap(stdout, heap);

    _free(test);

    destroy_heap(heap, HEAP_SIZE);

    puts("Delete the heap");
    debug_heap(stdout, heap);
}


void secondTest(){
    puts("try to release one of the blocks");
    void* heap = heap_init(HEAP_SIZE);

    if (heap == NULL){
        puts("fatal error when init the memory");
        return;
    }

    puts(" init the heap");
    debug_heap(stdout, heap);

    void* test1 = _malloc(MALLOC_2_TEST1);
    void* test2 = _malloc(MALLOC_2_TEST2);

    if (test1 == NULL || test2 == NULL){
        puts("Error while malloc the memory");
        return;
    }

    puts("After malloc for two tests");
    debug_heap(stdout, heap);

    _free(test1);

    puts("after release the first's test memory");
    debug_heap(stdout, heap);

    destroy_heap(heap, HEAP_SIZE);

    puts("release the memory");
    debug_heap(stdout, heap);

}


void thirdTest(){
    puts("release two blocks from several");
    void* heap = heap_init(HEAP_SIZE);

    if (heap == NULL){
        puts("fatal when init the heap");
        return;
    }

    puts("success init the heap");
    debug_heap(stdout, heap);

    void* test1 = _malloc(MALLOC_3_TEST1);
    void* test2 = _malloc(MALLOC_3_TEST2);
    void* test3 = _malloc(MALLOC_3_TEST3);

    if (test1 == NULL || test2 == NULL || test3 == NULL){
        puts("can't give the space in memory");
        return;
    }

    puts("after malloc");
    debug_heap(stdout, heap);

    _free(test1);

    puts("after free test1");
    debug_heap(stdout, heap);


    _free(test2);

    puts("after free test 2");
    debug_heap(stdout, heap);

    destroy_heap(heap, HEAP_SIZE);

    puts("after all free");
    debug_heap(stdout, heap);

}

void fourthTest(){
    puts("new region extend the previous");
    void* heap = heap_init(HEAP_SIZE);

    if (heap == NULL){
        puts("can't init the heap");
        return;
    }

    puts("init the heap");
    debug_heap(stdout, heap);

    void* test1 = _malloc(MALLOC_4_TEST1);
    void* test2 = _malloc(MALLOC_4_TEST2);

    if (test1 == NULL || test2 == NULL){
        puts("can't make a space for tests");
        return;
    }

    struct block_header* header1 = get_header(test1);
    struct block_header* header2 = get_header(test2);

    bool correct = (header1->capacity.bytes != MALLOC_4_TEST1) || (header1->next != test2) || (header2->capacity.bytes != MALLOC_2_TEST2);

    if (correct){
        puts("can't correct extend ");
        return;
    }

    _free(test1);

    puts("free test1");
    debug_heap(stdout, heap);

    _free(test2);

    puts("free test 2");
    debug_heap(stdout, heap);

    destroy_heap(heap, HEAP_SIZE);

}

void fifthTest(){
    puts("new region that dont extend");

    void* heap = heap_init(HEAP_SIZE_LAST);

    if (heap == NULL){
        puts("can't init the heap");
        return;
    }

    puts("init the heap");
    debug_heap(stdout, heap);

    void* test = _malloc(MALLOC_5_TEST);
    if (test == NULL){
        puts("can't malloc for the 1 test");
        return;
    }
    puts("init the 1 test");
    debug_heap(stdout, heap);

    struct block_header* header = get_header(test);

    if (header == NULL){
        puts("can't get a header for the 1 block");
        return;
    }

    debug_heap(stdout, heap);

    (void) mmap(header->contents + header->capacity.bytes, REGION_MIN_SIZE, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_FIXED, -1, 0);

    void* test2 = _malloc(MALLOC_5_TEST);

    if (test2 == NULL){
        puts("can't malloc for the 2 test");
        return;
    }

    puts("init the 2 test");
    debug_heap(stdout, heap);

    bool correct = get_header(test2) == (void*) (header->contents + header->capacity.bytes);

    if (correct){
        puts("can't extend");
        return;
    }

    _free(test);
    _free(test2);

    puts("after all free");
    debug_heap(stdout, heap);

    destroy_heap(heap, HEAP_SIZE_LAST);

}



